# imagen raiz
FROM node

#carpeta raiz,crea el directorio raiz
WORKDIR /tpbank

#copia de archivos de carpeta local a tpbank
ADD . /tpbank

#coge todas las dependencias del package.json y las instala
RUN npm install

#abrir el puerto 3000 que es el que hemos puesto en el codigo que el que queremos utilizar
EXPOSE 3000

# comando de inicializacion, que se lanzar cuando hacemos el start...
# cuando hagamos el build se ejecutan los RUN
# cuando hagamos el run, se ejecutan los comnando start

CMD ["npm", "start"]
