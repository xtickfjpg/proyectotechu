// constantes que son el core de la aplicacion
// nos traemos el framework... y lo ponemos en marcha
require('dotenv').config();
const express = require('express');
const app= express();

const userController = require('./controller/UserController'); // importar el userController operativa de usuario
const authController = require('./controller/AuthController');
const accountController = require('./controller/AccountController');
const movementController = require('./controller/MovementController');
const currencyQuotes = require('./controller/CurrencyQuotes');

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 // metemos esto para que permita pasar el JSON.
 res.set("Access-Control-Allow-Headers", "Content-Type");
//ojo si se usa el jwtoken hay que definir la variable XTOKEN.... si no no funciona
 next();
}

app.use(express.json());  // dentro del express.. para pasear el body y que utilice json
app.use(enableCORS);

// valor por defecto a una variable de entorno
// si existe la variable de entorno port.. cogemos ese valor
// sino cogemos el port 3000.
const port = process.env.PORT || 3000;
// pone en marcha eschucar el puerto
app.listen(port);
console.log("API escuchando en el puerto:" + port );

//*************************************
// API CONSULTA COTIZACIONES DE DIVISAS
//*************************************
app.get('/apitechu/v1/quotes',currencyQuotes.getQuotesV1);

//******************************************
// API CONSULTA USUARIOS TOTAL Y CON FILTROS
//******************************************
app.get('/apitechu/v1/users',userController.getUsersV1);
app.get('/apitechu/v1/users/:id',userController.getUsersByIdV1);

//********************************
// API ALTA NUEVO USUARIO
//********************************
app.post('/apitechu/v1/users',userController.createtUsersV1);

//************************
// API BORRADO USUARIO
//************************
//app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);
//************************
// API LOGIN
//************************
app.post('/apitechu/v1/login',authController.loginUsersV1);
//************************
// API logout
//************************
app.post('/apitechu/v1/logout/:id',authController.logoutUsersV1);
//********************************
// API PRUEBA DEVOLVER MSG
//********************************
app.get('/apitechu/v1/hello',
  function  (req,res) {
    console.log("GET /apitechu/v1/hello");
    //devolvemos un string que que podemos transaformar en json
    //res.send('{"msg" : "hola desde apitechu"}');
    //pero para devolver directamente en json tenemos que hacer
    // a partir de ahora.. todas las salidas de nuestros servicios tiene que
    // servicios asi.
    res.send({"msg" : "hola desde apitechu"});
  }
)
//********************************
// API CONSULTA DE CUENTAS
//********************************
app.get('/apitechu/v1/accounts/:user_id',accountController.getAcountsByUserIdV1);
app.post('/apitechu/v1/accounts',accountController.createAccountsV1);
app.post('/apitechu/v2/accounts',accountController.createAccountsV2);
app.post('/apitechu/v1/accounts/:user_id/:iban',accountController.cancelAccountsV1);

//********************************
// API CONSULTA DE MOVIMIENTOS
//********************************
app.get('/apitechu/v1/movements/:user_id/:iban',movementController.getMovementsV1);
app.post('/apitechu/v1/movements',movementController.createMovementV1);

//********************************
// API PRUEBA METODOS PARA PASAR LOS
// PARAMETROS AL SERVICIOS
//********************************
app.post(
  '/apitechu/v1/monstruo/:p1/:p2',
    function  (req,res) {
      console.log("proyectoTechu");
      console.log("GET /apitechu/v1/monstruo");
      console.log("parametros:");
      console.log(req.params);

      console.log("Query String:");
      console.log(req.query);

      console.log("Headers:");
      console.log(req.headers);

      console.log("Body:");
      console.log(req.body);
    }
)
