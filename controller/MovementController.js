
// para importar el io.js donde hemos metido la function de escribir fichero
const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";


//******************************************************************
// API CONSULTA MOVIMIENTOS POR ID DE USUARIO Y CUENTA CON BD MONGO
//******************************************************************

function getMovementsV1(req, res) {
  console.log("GET /apitechu/v1/movements/:user_id/:iban");
  var user_id = req.params.user_id;
  var iban = req.params.iban;
  console.log("user_id :" + user_id);
  console.log("iban :" + iban);

  var queryUser =  'q={"id":' + user_id + ',"logged": true}';
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + queryUser + "&" + mLabAPIKey,
     function(errUSER, resMLabUSER, bodyUSER) {
       if (errUSER) {
         res.status(500).send({"msg":"Error en acceso a BD users"});
       } else {
         if (bodyUSER.length > 0) {
           console.log ("usuario logado");
           var query = 'q={"user_id":' + user_id + ',"iban":"' + iban + '","status":{$ne:"cancelled"}}';
           console.log("query:"+query);
           var httpClient = requestJson.createClient(baseMlabURL);
           httpClient.get("accounts?" + query + "&" + mLabAPIKey,
              function(err, resMLab, body) {
                if (err) {
                  res.status(500).send({"msg": "error obteniendo movimientos de cuenta"});
                } else {
                  if (body.length > 0) {
                    console.log("cuenta encontrada: " + body.length);
                    var response = body[0].movement;
                    res.status(200).send(response);
                  } else {
                    res.status(404).send({"msg": "no existe la cuenta para el usuario"});
                  }
                }
              }
           )

         } else {
           console.log("usuario no logado");
           res.status(500).send({"msg":"Usuario no logado, volver a logarse"});
         }
       }
     }
  )
}

//******************************
// API ALTA NUEVO MOVIMIENTO
//******************************

function createMovementV1 (req, res) {
  console.log("POST /apitechu/v1/movements");
  console.log("usuario:" + req.body.user_id);
  console.log("iban:" + req.body.iban);
  console.log("concepto:"+req.body.concept);
  console.log("type:" + req.body.type);

  var queryUser =  'q={"id":' + req.body.user_id + ',"logged": true}';
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + queryUser + "&" + mLabAPIKey,
    function(errUSER, resMLabUSER, bodyUSER) {
      if (errUSER) {
        res.status(500).send({"msg":"Error en acceso a BD users"});
      } else {
        if (bodyUSER.length > 0) {
          console.log ("usuario logado");
          var query = 'q={"user_id":' + req.body.user_id + ',"iban":"' + req.body.iban + '","status":{$ne:"cancelled"}}';
          console.log("query:" + query);
          var httpClient = requestJson.createClient(baseMlabURL);
          httpClient.get("accounts?" + query + "&" + mLabAPIKey,
              function(errACC, resMLabACC, bodyACC) {
                if (errACC) {
                  res.status(500).send({"msg": "error obteniendo movimientos de cuenta"});
                } else {
                  if (bodyACC.length > 0) {
                    console.log("cuenta encontrada: " + bodyACC.length);
                    var movements = bodyACC[0].movement;
                    console.log(movements);
                    console.log("movimientos:" + bodyACC[0].movement);
                    var movement = {};
                      movement.date = getDate();
                      movement.concept = req.body.concept;
                      movement.type = req.body.type;
                      movement.amount = req.body.amount;
                      movement.mcurrency = req.body.mcurrency;
                      movements.push(movement);
                    console.log(movements);

                    // var auxBalance = 0;
                    // if (req.body.type == "+") {
                    //   auxBalance = bodyACC[0].balance+req.body.amount;
                    // } else {
                    //   auxBalance = bodyACC[0].balance-req.body.amount;
                    // }

                    if (req.body.type == "+") {
                      bodyACC[0].balance += req.body.amount;
                    } else {
                      bodyACC[0].balance -= req.body.amount;
                    }

                    console.log("balance:" + bodyACC[0].balance)

                    var stringMovements = JSON.stringify(movements);
                    // var stringBalance = JSON.stringify(auxBalance);

                    console.log("string :"+ stringMovements);

                    var putBody = '{"$set": {"movement":' + stringMovements + ',"balance":'+ bodyACC[0].balance + '}}';
                    console.log("putBody:"+ putBody);
                    httpClient.put("accounts?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                      function(errPUT, resMLabPUT, bodyPUT) {
                        if (errPUT) {
                          res.status(500).send({"msg" :"error alta movimiento al hacer el put"});
                        } else {
                          console.log("alta correcta de movimiento");
                          var respuesta = {};
                          respuesta.msg = "Alta correcta de movimiento";
                          respuesta.balance = bodyACC[0].balance;
                          res.status(201).send(respuesta);
                        }
                      }
                    )
                  } else {
                    res.status(404).send({"msg": "no existe la cuenta para el usuario"});
                  }
                }
              }
          )
        } else {
          console.log("usuario no logado");
          res.status(500).send({"msg":"Usuario no logado, volver a logarse"});
        }
      }
    }
  )
}
//********************************************
//  funcion obtener-fecha del dia
//********************************************

function getDate() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  return yyyy + '-' + mm + '-' + dd;
}

module.exports.getMovementsV1 = getMovementsV1;
module.exports.createMovementV1 = createMovementV1;
