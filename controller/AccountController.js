
// para importar el io.js donde hemos metido la function de escribir fichero
const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;
const todayKey = "key="+ process.env.CAMBIO;
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";
const basecambioTodayURL = "https://api.cambio.today/v1/quotes/EUR/USD/";


//users?q={"$and": [{"id":{"$gt":15}},{"id":{"$lt":20}}]}&f={"first_name":1, "_id":0,"id":1, }&apiKey=PqXm3N6BAr82mMt3KCpx5R6tjg3UwXpP";


//******************************************************
// API CONSULTA CUENTAS POR ID DE USUUARIO CON BD MONGO
//******************************************************

function getAcountsByUserIdV1(req, res) {
  console.log("GET /apitechu/v1/accounts/:user_id");
  var user_id = req.params.user_id;
  console.log("user_id :" + user_id);
  console.log("la id del usuario a consultar las cuentas:" + user_id);

  var queryUser = 'q={"id":' + user_id + ',"logged": true}';
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + queryUser + "&" + mLabAPIKey,
    function(errUSER, resMLabUSER, bodyUSER) {
      if (errUSER) {
        res.status(500).send({"msg":"Error en acceso a BD users"});
      } else {
        if (bodyUSER.length > 0) {
          console.log ("usuario logado");
          //var query = 'q={"user_id":' + user_id + ',"status":{$ne:"cancelled"}}';
          var query = 'q={"user_id":' + user_id + ',"status":{$ne:"cancelled"}}&s={"iban":1}';
          console.log("query:"+query);
          var httpClient = requestJson.createClient(baseMlabURL);
          httpClient.get("accounts?" + query + "&" + mLabAPIKey,
             function(err, resMLab, body) {
               if (err) {
                 res.status(500).send({"msg": "error obteniendo cuentas"});
               } else {
                 if (body.length > 0) {
                   console.log("usuario con " + body.length + " cuentas");
                   //
                   // recuperamos de la API cambio.today el contravalor a EUR de la divisa
                   var aux = {};
                   var httpClientQ = requestJson.createClient(basecambioTodayURL);
                   console.log('key: ' + basecambioTodayURL + "" + todayKey);
                   httpClientQ.get("json?quantity=1&" + todayKey,
                    function(errQ, resQ, bodyQ) {
                      console.log('resQ: ');
                      console.log(resQ);
                      aux = JSON.parse(resQ.body);
                      console.log('API cambio.today');
                      console.log('aux.result.value: ' + aux.result.value);
                      var response = body;
                      //recorremos el array de cuentas para calcular el contravalor
                      //en función de la divisa de la cuenta
                      for (var i = 0; i < response.length; i++) {
                        if (response[i].currency == "EUR") {
                           console.log('Cuenta en EUR');
                           response[i].countervalue = response[i].balance;
                           response[i].countercurrency = "EUR";
                        } else {
                          console.log('Cuenta en divisa ' + response[i].currency);
                          response[i].countervalue = (response[i].balance * aux.result.value).toFixed(2);
                          console.log('response[i].countervalue: ' + response[i].countervalue);
                          response[i].countercurrency = "EUR";
                        }
                      }
                      res.status(200).send(response);
                    }
                   )

                 } else {
                   res.status(404).send({"msg": "error cliente sin cuentas"});
                 }
               }
             }
          )
        } else {
          console.log("usuario no logado");
          res.status(500).send({"msg":"Usuario no logado, volver a logarse"});
        }
      }
    }
  )
}

//**********************************
// API ALTA NUEVA CUENTA DE USUARIO
//**********************************

function createAccountsV1 (req,res) {

  console.log("POST /apitechu/v1/accounts");
  console.log(req.body.user_id);
// alta de la nueva cuenta (user_id,iban,balance y movimiento)
  var newAccount = {};
  var iban = getIban();
  console.log ("iban:" + iban);
  newAccount.iban = iban;
  newAccount.user_id = req.body.user_id;
  newAccount.balance = 0;
//para pruebas damos de alta una cuenta en dólares
//  newAccount.currency = "EUR";
  newAccount.currency = "USD";
  newAccount.movement = [];
// alta del movimiento inicial de open account
  newMovement = {};
  newMovement.date = getDate();
  newMovement.concept = "OPEN ACCOUNT";
  newMovement.type = "+";
  newMovement.amount = +0;
//para pruebas damos de alta una cuenta en dólares
//  newMovement.mcurrency = "EUR";
  newMovement.mcurrency = "USD";
  newAccount.movement.push(newMovement);

  newAccount.status = "activated";

  console.log(newAccount);

  var httpClient = requestJson.createClient(baseMlabURL);

      httpClient.post("accounts?" + mLabAPIKey, newAccount ,
        function(err, resMLab, body) {
          console.log("cuenta dada de alta correctamente");
          res.status(201).send({
            "msg": "cuenta dada de alta correctamente",
            "iban" : iban
          });
        }
      )
}


function createAccountsV2 (req,res) {

  console.log("POST /apitechu/v2/accounts");
  console.log(req.body.user_id);
  console.log(req.body.currency);
// validación de la divisa. Debe ser EUR (euro) o USD (dólar)
  if ((req.body.currency!=="EUR") && (req.body.currency!=="USD")) {
    res.status(404).send({
      "msg": "ERROR- cuenta no es EUR ni USD"
    })
  } else {
    // alta de la nueva cuenta (user_id,iban,balance y movimiento)
      var newAccount = {};
      var iban = getIban();
      console.log ("iban:" + iban);
      newAccount.iban = iban;
      newAccount.user_id = req.body.user_id;
      newAccount.balance = 0;
    //para pruebas damos de alta una cuenta en dólares
    //  newAccount.currency = "EUR";
      newAccount.currency = req.body.currency;
      newAccount.movement = [];
    // alta del movimiento inicial de open account
      newMovement = {};
      newMovement.date = getDate();
      newMovement.concept = "OPEN ACCOUNT";
      newMovement.type = "+";
      newMovement.amount = +0;
    //para pruebas damos de alta una cuenta en dólares
    //  newMovement.mcurrency = "EUR";
      newMovement.mcurrency = req.body.currency;
      newAccount.movement.push(newMovement);

      newAccount.status = "activated";

      console.log(newAccount);

      var httpClient = requestJson.createClient(baseMlabURL);

          httpClient.post("accounts?" + mLabAPIKey, newAccount ,
            function(err, resMLab, body) {
              console.log("cuenta dada de alta correctamente");
              res.status(201).send({
                "msg": "cuenta dada de alta correctamente",
                "iban" : iban
              });
            }
          )
  }

}



function getIban() {
  var iban = "ES99 0182 4000 " + Math.round(getRandom(999,9999)) + " " +  Math.round(getRandom(999,9999)) + " " +  Math.round(getRandom(999,9999));
  return iban;
}

function getDate() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  return yyyy + '-' + mm + '-' + dd;
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

//************************************
// API BAJA LOGICA DE CUENTA DE USUARIO
//************************************

function cancelAccountsV1 (req,res) {

  console.log("POST /apitechu/v1/accounts/:user_id/:iban");

  console.log("parametros:");
  console.log(req.params);

  //newAccount.status = "activated";

  var httpClient = requestJson.createClient(baseMlabURL);
  var putBody = '{"$set": {"status":"cancelled"}}';
  var query = 'q={"user_id":' + req.params.user_id +',"iban":"' + req.params.iban +'"}';
  console.log("query:" + query);

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      if (errGET) {
        var response = {"msg" :"error peticion mlab al consultar usuario-cuenta"};
        res.status(500);
        res.send(response);
      } else {
        if (bodyGET.length > 0) {
          console.log("respuesta Mlab:"+resMLabGET);
          console.log("Recuperado:" + bodyGET.length);
          console.log("iban:"+bodyGET[0].iban);
          console.log("usuario:"+bodyGET[0].user_id);
          console.log("balance:"+bodyGET[0].balance);

          httpClient.put("accounts?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              if (errPUT) {
                var response = {"msg" :"error cancelar cuenta al hacer el put"};
                res.status(500);
              } else {
                console.log("CANCELACION CORRECTA");
                var response = {};
                response.mensaje = "cancelacion correcta de la cuenta";
                response.iban = bodyGET[0].iban;
                response.user_id = bodyGET[0].user_id;
                console.log("response:" + response.mensaje + " " + response.iban + " " + response.user_id);
                res.send(response);
              } //fin OK
            } // fin-function put-mlab
          )
        } else {
          var response = {"msg" :"usuario/cuenta no encontrado"};
          res.status(404);
          res.send(response);
        } // fin if-
      }
    }
  )

} // fin-cancelAccount


module.exports.getAcountsByUserIdV1 = getAcountsByUserIdV1;
module.exports.createAccountsV1 = createAccountsV1;
module.exports.createAccountsV2 = createAccountsV2;
module.exports.cancelAccountsV1 = cancelAccountsV1;
