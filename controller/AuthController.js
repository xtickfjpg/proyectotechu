
// para importar el io.js donde hemos metido la function de escribir fichero
const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";


//******************************************
// API REALIZAR EL LOGIN Y LOGOUT MONGO
//******************************************

function loginUsersV1 (req, res) {

  console.log("POST /apitechu/v1/login");
  console.log("proyectoTechu LOGIN");
  console.log(req.body.email);
  console.log(req.body.password);

  //recogemos las variables del
  var email=req.body.email;
  var password=req.body.password;

  var httpClient = requestJson.createClient(baseMlabURL);
  var putBody = '{"$set": {"logged":true}}';
  var query = 'q={"email":' + '"' + email + '"'+ '}';
  console.log(query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      console.log("dentro de la consulta de usuario");
      //console.log(resMLabGET);
      if (errGET) {
          console.log("error 1 -GET");
          var response = {"msg" :"error al hacer loging"};
          res.status(500);
          res.send(response);
      } else {
            console.log("body recuperado : " + bodyGET.length);
            if (bodyGET.length == 0 ) {
            var response = {"msg" :"usuario no encontrado"};
            res.status(404);
            res.send(response);
          } else {
            console.log("por ok");
            console.log("password de entrada:" + req.body.password );
            console.log("password recuperada:" + bodyGET[0].password);
            console.log("llamada a bcrypt"  + crypt.checkPassword(req.body.password,bodyGET[0].password));
            if (crypt.checkPassword(req.body.password,bodyGET[0].password)) {
              console.log("usuario encontrado y pw ok");
              httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT) {
                  if (errPUT) {
                    var response = {"msg" :"error login al hacer el put"};
                    res.status(500);
                  } else {
                    console.log("LOGIN CORRECTO");
                    var response = {};
                    response.mensaje = "Login correcto";
                    response.idUsuario = bodyGET[0].id;
                    console.log("response:" + response.mensaje + response.idUsuario);
                    res.send(response);
                  }
                }
              )
            } else {
              console.log("salgo por error de pw");
              var response = {};
              response.mensaje = "Login incorrecto";
              res.status(404);
              res.send(response);
            }
          }
      }
    }
  )
}

function logoutUsersV1 (req, res) {

  console.log("POST /apitechu/v1/logout");

  var httpClient = requestJson.createClient(baseMlabURL);
  var putBody = '{"$unset": {"logged":""}}';
  var query = 'q={"id":' + req.params.id + '}';
  console.log(query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      if (errGET) {
        var response = {"msg" :"error peticion mlab al hacer el get del logout"};
        res.status(500);
        res.send(response);
      } else {
        if (bodyGET.length > 0 ) {
          console.log("id encontrado");
          httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
                console.log("logout CORRECTO");
                var response = {};
                response.mensaje = "Logout correcto";
                response.idUsuario = req.params.id;
                console.log("response:" + response.mensaje + response.idUsuario);
                res.send(response);
            }
          )
        } else {
          console.log("usuario no encontrado");
          var response = {"msg":"logout incorrecto"};
          res.status(404);
          res.send(response);
        }

      }

    }
  )
}


module.exports.loginUsersV1 = loginUsersV1;
module.exports.logoutUsersV1 = logoutUsersV1;
