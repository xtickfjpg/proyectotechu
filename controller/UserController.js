
// para importar el io.js donde hemos metido la function de escribir fichero
const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";

//******************************************
// API CONSULTA USUARIOS TOTAL CON BD MONGO
//******************************************

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
          var response =
          {
            "msg" :"error obteniendo usuario"
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              var response = body;
            } else {
              var response = {
                "msg" :"error obteniendo usuario"
              }
              res.status(404);
            }
          }
          res.send(response);
        }

  )
}

//******************************************
// API CONSULTA USUARIOS POR ID CON BD MONGO
//******************************************

function getUsersByIdV1(req, res) {
  console.log("GET /apitechu/v1/users/:id");
  var id = req.params.id;
  console.log("la id del usuario a consultar:" + id);
  var query = 'q={"id":' + id + '}';
  console.log('query: ' + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
          console.log('ERROR- ' + err);
          var response =
          {
            "msg" :"error obteniendo usuario"
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              var response = {
                "msg" :"error obteniendo usuario"
              }
              res.status(404);
            }
          }
          res.send(response);
    }
  )
}

//***************************************
// API ALTA DE USUARIO ASIGNANDO ID AUTO
//***************************************

function createtUsersV1 (req, res) {
  console.log("POST /apitechu/v1/users");

//  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var httpClient = requestJson.createClient(baseMlabURL);

  // Consultamos todos los usuarios dados de alta para calcular el máximo id
  // y además validamos que el email de entrada no existe en la base de datos
  httpClient.get("users?" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      if (errGET) {
        var response =
        {
          "msg" :"error obteniendo usuarios"
        }
        res.status(500);
        res.send(response);
      } else {
          if (bodyGET.length > 0) {
            var max = 0;
            var emailIN = req.body.email;
            var emailexiste = false;
            console.log('max Id: ' + max);
            console.log('email entrada: ' + emailIN);

            // INICIO del FOR
            // recorremos el array para calcular el máximo id
            // y validamos que el email de entrada no exista ya
            for (var i = 0; i < bodyGET.length; i++) {
              if (bodyGET[i].id > max) {
                max = bodyGET[i].id;
//                console.log('Cambia el max Id: ' + max);
              };
//              console.log('compara email');
//              console.log('bodyGET[i].email: ' + bodyGET[i].email);
//              console.log('emailIN: ' + emailIN);
              if (bodyGET[i].email == emailIN) {
                // ERROR el email ya existe
                emailexiste = true;
                console.log('error - el email ' + emailIN + ' ya existe');
              };
            }
            // FIN del FOR
          } else {
            // si no hay usuarios dados de alta asignamos el id=1 y continuamos
            var max = 1;
            console.log('No hay usuarios en la base de datos -> id=1');
          }
        }
        if (emailexiste) {
          var response =
          {
            "msg" :"error - el email ya existe"
          }
          res.status(401);
          res.send(response);
        }
        else {
          // se da de alta el nuevo usuario
          // +1 al último Id recuperado
          max++;
          console.log('Id: ' + max + ' asignado, preparado para insertar nuevo User');

          // se crea nuevo usuario
          var newUser = {};
          newUser.id = max;
          newUser.first_name = req.body.first_name;
          newUser.last_name = req.body.last_name;
          newUser.email = req.body.email;
          newUser.password = crypt.hash(req.body.password);

          console.log('newUser.id: ' + newUser.id);
          console.log('newUser.first_name: ' + newUser.first_name);
          console.log('newUser.last_name: ' + newUser.last_name);
          console.log('newUser.email: ' + newUser.email);
          console.log("pw encriptado :" + newUser.password);

          // se inserta usuario en la base de datos
          httpClient.post("users?" + mLabAPIKey, newUser ,
            function(errPOST, resMLabPOST, bodyPOST) {
              console.log("usuario dado de alta correctamente");
              var response =
              {
                "msg" :"usuario dado de alta correctamente"
              }
              res.status(201);
              res.send(response);
            }
          )
        }
      }
  )
}


module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersByIdV1 = getUsersByIdV1;
module.exports.createtUsersV1 = createtUsersV1;
